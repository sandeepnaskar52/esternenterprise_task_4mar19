<?php

require "Calculator.php";

	if(strpos($argv[2],'-')==false){
		if ($argv[1] == 'add' ) {
			$Calculate = new Calculator;
			$Calculate->setValues($argv[2]);
			$add = $Calculate->explodeArray();
			$result =$Calculate->addition($add);
			print_r($result);
		}elseif ($argv[1] == 'multiply' ) {
			$Calculate = new Calculator;
			$Calculate->setValues($argv[2]);
			$multiply = $Calculate->explodeArray();
			$result =$Calculate->multiply($multiply);
			print_r($result);
		} else {
			die("Please try again");
		}
	}else{
		
		$values = preg_replace('/[^\d-]+/', ',', $argv[2]); 
		$printArray  = explode(',', $values);
		
		$neg_arr = array();
		foreach($printArray as $val)
		{
		    if($val<0){
		    	$neg_arr[]=$val;
		    }  
		}

		$negValue = json_encode($neg_arr);		
		echo "Negative numbers ".$negValue." not allowed.";		
	}