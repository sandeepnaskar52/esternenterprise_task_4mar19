<?php 

Class Calculator {

	var $val;

	function setValues($value){
		$this->$val = $value;
	}

	function addition($additionValues){
		if(is_array($additionValues)){
	
			return array_sum($additionValues);
		}else{
	
			return $additionValues;
		}
	}

	function explodeArray()
	{
		if($this->$value !=='' || $this->$value!==null){
			$values = preg_replace("/[^0-9]/", ',', $this->$value);
	     		return explode(',', $values);
			}else{
				if($this->$value==''){
					return '0';
				}
				return $this->$value;
			}			
	}
}
if(strpos($argv[2],'-')==false){
	$Calculate = new Calculator;
	$Calculate->setValues($argv[2]);
	$add = $Calculate->explodeArray();
	$result =$Calculate->addition($add);
	print_r($result);
}else{
	
	$values = preg_replace('/[^\d-]+/', ',', $argv[2]); 
	$printArray  = explode(',', $values);
	
	$neg_arr = array();
	foreach($printArray as $val)
	{
	    if($val<0){
	    	$neg_arr[]=$val;
	    }  
	}

	$negValue = json_encode($neg_arr);
	
	echo "Negative numbers ".$negValue." not allowed.";
	
}